#!/bin/bash

EXECUTOR = $1
URL = $2
TOKEN = $3
EXECUTOR_IMAGE = $4
TLS_CA_FILE = $5

_TAG_LIST = 'ci,docker,vbox,vagrant'

if [[ $EXECUTOR = 'shell' ]]; then
  gitlab-ci-multi-runner register --executor shell --non-interactive \
                                  --url $URL -r $TOKEN \
                                  --tls-ca-file $TLS_CA_FILE \
                                  --tag-list $_TAG_LIST

elif [[ $EXECUTOR = 'docker' ]]; then
  gitlab-ci-multi-runner register --executor docker --non-interactive \
                                  --url $URL --registration-token $TOKEN \
                                  --tls-ca-file $TLS_CA_FILE \
                                  --tag-list $_TAG_LIST \
                                  --docker-pull-policy if-not-present \
                                  --docker-image $EXECUTOR_IMAGE

else
  echo "Executor não parametrizado"
  exit 1
fi
