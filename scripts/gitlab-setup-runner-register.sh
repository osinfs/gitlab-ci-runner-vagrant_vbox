#!/bin/bash
# :args = ["#{URL_GIT_SERVER}", "#{TOKEN}"]
URL_GIT_SERVER=$1
TOKEN=$2
CA_CHAIN=$3
RUNNER_LIMIT=$4
DOCKER_AUTH_CONFIG=$5
DOCKER_REGISTRY_URL=$6

gitlab-ci-multi-runner register \
  --env "DOCKER_AUTH_CONFIG={\"auths\": {\"registry-git.prevnet\": {\"auth\": \"$DOCKER_AUTH_CONFIG\"}}}" \
  --non-interactive --url https://$URL_GIT_SERVER/ci --registration-token $TOKEN \
  --executor docker \
  --tls-ca-file /etc/gitlab-runner/$CA_CHAIN \
  --tag-list ci,docker,vbox,vagrant \
  --docker-pull-policy always \
  --docker-privileged \
  --docker-image ruby:2.1
