#!/bin/bash -x

echo "INFO: [docker-compose.sh] build simple flask image"
if [[ "$HTTP_PROXY" != "" && "$HTTP_PROXY" != "http://proxy_not_set:3128" ]]; then
  ENVIRONMENT="-e NO_PROXY='localhost,127.0.0.1,.nuvemdtp,.prevnet,.hacklab'
               -e no_proxy='localhost,127.0.0.1,.nuvemdtp,.prevnet,.hacklab'
               -e HTTPS_PROXY=$HTTP_PROXY
               -e HTTP_PROXY=$HTTP_PROXY
               -e https_proxy=$HTTP_PROXY
               -e http_proxy=$HTTP_PROXY"
fi

docker run $ENVIRONMENT $1
