#!/bin/bash

echo "INFO: [docker-compose.sh] build simple flask image"
if [[ "$HTTP_PROXY" != "" && "$HTTP_PROXY" != "http://proxy_not_set:3128" ]]; then
  ENVIRONMENT="--build-arg NO_PROXY='localhost,127.0.0.1,.nuvemdtp,.prevnet,.hacklab'
               --build-arg no_proxy='localhost,127.0.0.1,.nuvemdtp,.prevnet,.hacklab'
               --build-arg https_proxy=$HTTP_PROXY
               --build-arg http_proxy=$HTTP_PROXY"
fi

docker build $ENVIRONMENT -t registry.gitlab.com/dataprev/puppet-tests$1 .
