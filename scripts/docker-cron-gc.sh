#!/bin/bash

echo '# Docker GC' >> /etc/crontab
echo '10 * * * * root /bin/docker ps -qa -f status=exited | xargs --no-run-if-empty /bin/docker rm -v' >> /etc/crontab
echo '10 23 * * * root /bin/docker images -qaf dangling=true | xargs --no-run-if-empty /bin/docker rmi' >> /etc/crontab
echo '10 23 * * * root /bin/docker volume ls -f dangling=true -q | xargs --no-run-if-empty /bin/docker volume rm' >> /etc/crontab
