#!/bin/bash
# gitlab-runner add ca-cert
#cp /vagrant/setup/ca-certs/* /etc/gitlab-runner/certs
cp /vagrant/setup/ca-certs/* /etc/gitlab-runner/

# docker add ca-cert
mkdir -p /etc/docker/certs.d/registry-git.prevnet
cat /vagrant/setup/ca-certs/*.crt >> /etc/docker/certs.d/registry-git.prevnet/ca.crt
systemctl restart docker
