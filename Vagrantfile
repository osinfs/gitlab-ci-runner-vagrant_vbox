# -*- mode: ruby -*-
# vi: set ft=ruby :
'''
/**
 * GitLab-CI Runners
 *
 * Vagrantfile of Runners for GitLab-CI
 * plugins:
 * - vagrant-proxyconf
 * - vagrant-triggers (opcional)
 *
 * `$ vagrant plugin install vagrant-triggers`
 *
 * @author Adriano Vieira <adriano.svieira at gmail.com>

   Copyright 2016 Adriano dos Santos Vieira

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 * @license @see LICENCE
 */
'''
require 'socket'
VM_HOST = Socket.gethostname
# Número de nodes para runners (default: 1)
NODES = (ENV.key?('NODES') ? ENV['NODES'].to_i : 1)
# gitlab server name
URL_GIT_SERVER = (ENV.key?('URL_GIT_SERVER') ? ENV['URL_GIT_SERVER'] : 'www-scm.prevnet')
# gitlab project token
TOKEN = (ENV.key?('TOKEN') ? ENV['TOKEN'] : 'xLAdJyNGs6HVzTnxjxwg')
# gitlab ca-chain file name
CA_CHAIN = (ENV.key?('CA_CHAIN') ? ENV['CA_CHAIN'] : 'dtp-ca-chain.crt')
# gitlab runners build limitations
RUNNER_LIMIT = (ENV.key?('RUNNER_LIMIT') ? ENV['RUNNER_LIMIT'] : 1)
# gitlab runners docker token to pull|push images
DOCKER_AUTH_CONFIG = (ENV.key?('DOCKER_AUTH_CONFIG') ? ENV['DOCKER_AUTH_CONFIG'] : '')
# docker images registry
DOCKER_REGISTRY_URL = (ENV.key?('DOCKER_REGISTRY_URL') ? ENV['DOCKER_REGISTRY_URL'] : 'registry-git.prevnet')

# [TBA] daemon que irá executar os jobs especificados no Gitlab-CI
EXECUTOR = (ENV.key?('EXECUTOR') ? ENV['EXECUTOR'] : 'docker')
EXECUTOR_IMAGE = (ENV.key?('EXECUTOR_IMAGE') ? ENV['EXECUTOR_IMAGE'] : 'ruby:2.1')

Vagrant.configure("2") do |config|
  config.vm.box = "dataprev/redhat7_x64_minimal-docker-gitlab_runner"
  config.vm.synced_folder './', '/vagrant'

  config.vm.provision "docker-setup-proxy", type: "shell",
          path: "scripts/docker-setup-proxy.sh"

  config.vm.provision "crontab-add-docker-gc", type: "shell",
          path: "scripts/docker-cron-gc.sh"

  config.vm.provision "ca-cert-add", type: "shell",
          path: "scripts/ca-cert-add.sh"

  config.vm.provision "gitlab-setup-runner-register", type: "shell",
          path: "scripts/gitlab-setup-runner-register.sh",
          args: [URL_GIT_SERVER, TOKEN, CA_CHAIN, RUNNER_LIMIT, DOCKER_AUTH_CONFIG, DOCKER_REGISTRY_URL]

  config.vm.provider "virtualbox" do |virtualbox| # general Virtualbox.settings
    virtualbox.customize [ "modifyvm", :id, "--cpus", 2 ]
    virtualbox.customize [ "modifyvm", :id, "--memory", 2048 ]
    virtualbox.customize [ "modifyvm", :id, "--name", 'vm.prevnet' ]
    virtualbox.customize [ "modifyvm", :id, "--groups", "/gitlab-runner" ]
  end # end Virtualbox.settings

  config.vm.provider :libvirt do |libvirt|
    libvirt.cpus = 2
    libvirt.memory = 4096
  end

  (1..(NODES)).each do |node_id|
    config.vm.define "#{VM_HOST}-runner-node-#{node_id}" do |node|  # define-VM
      node.vm.hostname = "#{VM_HOST}-runner-node-#{node_id}.prevnet"

      node.vm.provider "virtualbox" do |virtualbox| # Virtualbox.settings
        virtualbox.customize [ "modifyvm", :id, "--name", "#{VM_HOST}-runner-node-#{node_id}.prevnet" ]
      end # end Virtualbox.settings

      # opcional para remover runner do projeto no gitlab (só remove de um por enquanto)
      if Vagrant.has_plugin?("vagrant-triggers")
        node.trigger.before :destroy do
          info "ungeristering gitlab-runner"
          run_remote  "gitlab-ci-multi-runner unregister --name #{VM_HOST}-runner-node-#{node_id}.prevnet"
        end
      end

    end # end-of-define-VM
  end # end-of-define-VM-loop node_id

end
